
// Global channel

var eventBus = new Vue()

// PRODUCT DATA

Vue.component('product', {
	props: {
		premium: {
			type: Boolean,
			required: true
		}
	},
	template: `<div class="product">

			<div class="product-image">
				<img :src="image">
			</div>

			<div class="product-info">
				 <h1>{{ product }}</h1>

				<span>{{ description}} </span> <span style="color: red" v-show="!inStock"><b>Out of stock!</b></span>

				<p>
				<a :href="link" target="_blank">Link</a>
				<p>

				<span v-show="onSale">On Sale!</span>
				

				<p>Shipping: {{ shipping }}</p>
				<p>Available sizes:</P>

				 <ul>
					<li v-for="variant in variants" :key="variants.variantID">{{ variant.variantSize }}</li>
				 </ul>

				 <div v-for="(variant, index) in variants" 
					:key="variants.variantID"
					class="colour-box"
					:style="{ backgroundColor: variant.variantColour } "
					@mouseover="updateProduct(index)">
				 </div>
				 <div>
				  <button v-on:click="addToCart"
				  :disabled="!inStock"
				  :class="{ disabledButton: !inStock }">Add to Cart</button>
                  <button @click="removeFromCart">Remove</button>
                 </div>	

                </div>

                <product-tabs :reviews="reviews" style="padding-top: 20px"></product-tabs>

              </div>
              `,
		data () {
			return {
				product: 'Socks',
				brand: 'Snazzy',
				description: 'These socks go on your feet and are fuzzy and warm',
				selectedVariant: 0,
				link: 'https://www.nicoladesign.co.uk/',
				onSale: false,
				variants: [
					{
						variantID: 1234,
						variantColour: "green",
						variantSize: 7,
						variantImage: "https://media2.newlookassets.com/i/newlook/803285638/mens/clothing/underwear-and-socks/dark-green-smile-embroidered-socks.jpg?strip=true&qlt=80&w=720",
						variantQty: 10
					},
					{ 		
						variantID: 1234,
						variantColour: "blue",
						variantSize: 8,
						variantImage: "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.stuartslondon.com%2Fimages%2Fburlington-lord-blue-sock-210216046-p25354-255924_image.jpg&f=1&nofb=1",
						variantQty: 0
					}
		        ],
		        reviews: []
		    }
		},
		methods: {
		updateProduct: function (index) {
			this.selectedVariant = index
			console.log(index)
		},
		removeFromCart: function () {
			this.cart -= 1
		},
		addToCart: function () {
			this.$emit('add-to-cart', this.variants[this.selectedVariant].variantID )
		},
		removeFromCart: function() {
             this.$emit('remove-from-cart', this.variants[this.selectedVariant].variantID)
             console.log(this.variants[this.selectedVariant].variantID)
        }
	},
	computed: {
		title() {
			return this.brand + ' ' + this.product
		},
		image () {
			return this.variants[this.selectedVariant].variantImage
		},
		inStock() {
			return this.variants[this.selectedVariant].variantQty
		},
		shipping() {
			if (this.premium) {
				return "Free"
			}
			return 2.99
		}
	},
	mounted()	{
		eventBus.$on('review-submitted', productReview =>{
			this.reviews.push(productReview)
		})
	}

})

// PRODUCT REVIEW DATA

Vue.component('product-review', {
	template: `
      <form class="review-form" @submit.prevent="onSubmit">
      <p>
        <label for="name">Name:</label>
        <input id="name" v-model="name" placeholder="name">
      </p>
      
      <p>
        <label for="review">Review:</label>      
        <textarea id="review" v-model="review" placeholder="Please add your review"></textarea>
      </p>
      
      <p>
        <label for="rating">Rating:</label>
        <select id="rating" v-model.number="rating">
          <option>5</option>
          <option>4</option>
          <option>3</option>
          <option>2</option>
          <option>1</option>
        </select>
      </p>
          
      <p>
        <input type="submit" value="Submit">  
      </p>    
    
    </form>
	`,
	data() {
		return {
			name: null,
			review: null,
			rating: null
		}
	},
	methods:{
		onSubmit() {
			let productReview = {
				name: this.name,
				review: this.review,
				rating: this.rating

			}
			eventBus.$emit('review-submitted', productReview)
			this.name = null
			this.review = null
			this.rating = null
		}
	}
		
})

// REVIEWS TABS

Vue.component('product-tabs', {
	props: {
		reviews: {
			type: Array,
			required: true
		}
	},
	template: `
		<div>
			<div>
				<span class="tab"
				:class="{ activeTab: selectedTab === tab}"
				 v-for="(tab, index) in tabs" 
				:key="index"
				@click="selectedTab = tab">
				{{ tab }}</span>
			</div>
			

			<div v-show="selectedTab === 'Reviews'">
		        <p v-if="!reviews.length">There are no reviews yet.</p>
		        <ul v-else>
		          <li v-for="(review, index) in reviews" :key="index">
		          <p>{{ review.name }}</p>
		          <p>Rating: {{ review.rating }}</p>
		          <p>{{ review.review }}</p>
		          </li>
		        </ul>
		     </div>

           <div>
	        <product-review v-show="selectedTab === 'Make a review'"
	        ></product-review>
	       </div>

       </div>
	`,
      data() {
      	return{
      		tabs: ['Reviews', 'Make a review'],
      		selectedTab: 'Reviews'
      	}
      }
 
	})


// GLOBAL DATA

var app = new Vue ({
	el: '#app',
	data: {
		premium: true,
		cart: []
	},
	methods: {
		updateCart: function (id) {
			this.cart.push(id)
		},
        removeItem(id) {
         for(var i = this.cart.length - 1; i >= 0; i--) {
         if (this.cart[i] === id) {
         this.cart.splice(i, 1);
                  }
               }
            }
	}
	
})